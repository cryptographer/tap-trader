const STATE = {
    MENU: 0,
    RUNNING: 1,
}

class Point {
    constructor(x, value) {
        this.x = x;
        this.value = value;
    }
}

export default class Game {
    constructor() {
        this.state = STATE.MENU;
        this.backgroundColor = 'lightblue';
        this.score = 1000;
        this.timer = 60;
        this.pointsArray = [];
    }

    buyPrice;
    sellPrice;
    numberOfShares;
    finalScore;

    init(context) {
        this.pointsArray.push(new Point(window.innerWidth / 2, 200));

        window.setInterval(this.updateStock.bind(this), 20);
        window.setInterval(this.updateTimer.bind(this), 1000);

        let font = new FontFace('FiraMono-Bold', 'url(fonts/FiraMono-Bold.woff2)');
        font.load().then(function() {
            document.fonts.add(font);
            context.font = '32px FiraMono-Bold';
        });

        let success = new Audio('audio/success.mp3');
        let fail = new Audio('audio/fail.mp3');

        window.addEventListener('pointerdown', function (event) {
            if(event.button !== 0) return;

            if(this.state === STATE.RUNNING) {
                this.buyPrice = this.pointsArray[this.pointsArray.length - 1].value;
                this.numberOfShares = Math.floor(this.score / this.buyPrice);
                console.log('Buy Price: ', this.buyPrice, 'Number of Shares: ', this.numberOfShares);
            }
        }.bind(this));

        window.addEventListener('pointerup', function (event) {
            if(event.button !== 0) return;

            if(this.state === STATE.MENU) {
                this.score = 1000;
                this.timer = 60;
                this.state = STATE.RUNNING;
                return;
            }

            this.backgroundColor = 'lightblue';

            this.sellPrice = this.pointsArray[this.pointsArray.length - 1].value;
            console.log('Sell Price: ', this.sellPrice);

            if(this.sellPrice - this.buyPrice >= 0) {
                success.load();
                success.play();
            } else {
                fail.load();
                fail.play();
            }

            this.score += this.numberOfShares * (this.sellPrice - this.buyPrice);

            if(this.score <= 0) {
                this.state = STATE.MENU;
            }

            this.buyPrice = null;
            this.sellPrice = null;
            this.numberOfShares = null;
        }.bind(this));
    }

    updateStock() {
        let nextElement;
        do {
            nextElement = this.pointsArray[this.pointsArray.length - 1].value * (10 + (Math.random() - 0.49)) / 10;
            nextElement = Math.round(nextElement + Number.EPSILON);
        } while (nextElement > 1000);

        let newPoint = new Point(window.innerWidth / 2, nextElement);
        this.pointsArray.push(newPoint);
    }

    updateTimer() {
        if(this.timer === 0) {
            this.state = STATE.MENU;
            this.buyPrice = null;
            this.sellPrice = null;
            this.numberOfShares = null;
            this.backgroundColor = 'lightblue';
            this.timer = 60;
            this.finalScore = this.score;
        } else if(this.state === STATE.RUNNING) {
            this.timer -= 1;
        }
    }

    draw(context) {
        context.fillStyle = 'black';
        context.textAlign = 'center';
        context.font = '32px FiraMono-Bold';
        if(this.state === STATE.RUNNING) {
            context.fillText('$' + this.score, window.innerWidth / 2, 40);
            context.fillText(this.timer, window.innerWidth / 2, 120);
        }

        if(this.buyPrice) {
            context.textAlign = 'center';
            context.fillText('Bought ' + this.numberOfShares + ' Shares @ $' + this.buyPrice, window.innerWidth / 2, 80);
        }

        // Draw chart data
        context.lineWidth = 2;
        context.beginPath();
        this.pointsArray.forEach(point => {
            if(point.x < -4) {
                this.pointsArray.shift();
            } else {
                if(this.buyPrice) {
                    if(point.value > this.buyPrice) {
                        context.strokeStyle = 'green';
                    } else {
                        context.strokeStyle = 'red';
                    }
                } else {
                    context.strokeStyle = 'black';
                }

                context.lineTo(point.x, window.innerHeight - (point.value/1000) * (window.innerHeight * 0.8));
                point.x -= 4;
            }
        });
        context.stroke();

        // Draw menu screen
        if(this.state === STATE.MENU) {
            context.fillStyle = 'rgba(0, 0, 0, 0.9)';
            context.fillRect(0, 0, window.innerWidth, window.innerHeight);

            context.fillStyle = '#ffffff';
            context.textAlign = 'center';

            context.font = '64px FiraMono-Bold';
            context.fillText('Tap Trader', window.innerWidth / 2, 100);

            if(this.finalScore) {
                context.font = '32px FiraMono-Bold';
                context.fillText('Final score: $' + this.finalScore, window.innerWidth / 2, 200);
            }

            context.font = '32px FiraMono-Bold';
            context.fillText('Click to Start', window.innerWidth / 2, window.innerHeight / 2);
            context.fillText('Hold to Buy | Release to Sell', window.innerWidth / 2, window.innerHeight - 100);
        }
    }

    drawGrid(context) {
        context.fillStyle = this.backgroundColor;
        context.fillRect(0, 0, window.innerWidth, window.innerHeight);

        const chartScale = 0.8;
        let chartHeight = window.innerHeight * chartScale;
        let majorDivisionSpacing = chartHeight / 10;
        let minorDivisionSpacing = chartHeight / 50;

        context.beginPath();
        context.lineWidth = 4;
        context.textAlign = 'right';
        context.fillStyle = 'black';
        context.strokeStyle = 'black';
        for(let i = 0; i < 11; i++) {
            context.moveTo(0, window.innerHeight - (i * majorDivisionSpacing));
            context.lineTo(window.innerWidth, window.innerHeight - (i * majorDivisionSpacing));

            context.font = '16px FiraMono-Bold';
            context.fillText('$' + (i * 100).toString(), window.innerWidth - 10, (window.innerHeight - 5) - (i * majorDivisionSpacing));
        }
        context.stroke();

        context.beginPath();
        context.lineWidth = 1;
        for(let i = 0; i < 51; i++) {
            context.moveTo(0, window.innerHeight - (i * minorDivisionSpacing));
            context.lineTo(window.innerWidth, window.innerHeight - (i * minorDivisionSpacing));
        }
        context.stroke();
    }
}