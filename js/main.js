import Game from './game.js';

const canvas = document.getElementById('canvas');
const context = canvas.getContext('2d');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

window.addEventListener('resize', function (event) {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
});

let game = new Game();

function animate() {
    game.drawGrid(context);
    game.draw(context);

    window.requestAnimationFrame(animate);
}

game.init(context);
animate();