# Tap Trader

This is a stock trading game written in javascript and rendered using the canvas API, you can play it [here](https://cryptographer.gitlab.io/tap-trader/). 

The sound effects were created by [Bertrof](https://freesound.org/people/Bertrof/sounds/131660/) and are licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/), and the font is [Fira Mono](https://github.com/mozilla/Fira) licensed under the [SIL Open Font License Version 1.1](https://scripts.sil.org/OFL).